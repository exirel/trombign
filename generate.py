"""Generate HTML files for Agone LARP.

Usage
-----

Use this file as a command line::

    $ python generate.py -i path/to/characters.csv -o path/to/output

To get help and usage::

    $ python generate.py --help

The output dir, by default, is the ``html`` folder. This can be modified with
the ``-o/--output OUTPUT_DIR`` option.


Charset Encoding
----------------

If the characters.csv file is in Windows 1252 charset (cp1252 in python), the
``--charset`` option can be used::

    $ python generate.py -i path/to/characters.csv --charset=cp1252

Common charset can be:

* utf-8 (UTF-8)
* iso-8859-15 (latin 1)
* cp1252 (Windows)

You can also use iconv to modify the charset of a given filename.
"""
import argparse
import csv
import html
import os
import re
import shutil

photo_re = re.compile(r'^(?P<id_player>\w-[\d-]+)')

CSV_HEADERS = (
    'mini_bg_sent',
    'bg_sent',
    'id_trombi',
    'id_player',
    'group',
    'last_name',
    'first_name',
    'email',
    'chara_name',
)
"""List of CSV header name.

Declare each field in the same order as given in the CSV file. Extra fields
are ignored by the CSV reader.

It's taken from the original file::

    Mini- BG en- voyé
    BG en- voyé
    Code trombi
    NomCode
    Sous-groupe
    name (last name)
    player_namer (first name)
    Mail du joueur
    Nom du personnage

"""

TEMPLATE_PLAYERS = """<!DOCTYPE html>
<html>
    <head>
        <title>PLAYERS Face Book Agone LARP</title>
        <link href="bootstrap.min.css" rel="stylesheet" />
        <link href="all.css" rel="stylesheet" />
        <meta charset="utf-8">
    </head>
    <body>
        <div class='container_fluid'>
          <div class='no-details row trombi'>
              {content}
          </div>
        </div>
    </body>
</html>
"""


TEMPLATE_ORGAS = """<!DOCTYPE html>
<html>
    <head>
        <title>ORGA Face Book Agone LARP</title>
        <link href="bootstrap.min.css" rel="stylesheet" />
        <link href="all.css" rel="stylesheet" />
        <meta charset="utf-8">
    </head>
    <body>
        <div class='container_fluid'>
          <div class='details row trombi'>
              {content}
          </div>
        </div>
    </body>
</html>
"""


TEMPLATE_LINE_PLAYER = """
<div class="col-sm-2 trombi-entry">
    <div class="thumbnail">
        <img src="{photo}" alt="missing" />
        <div class="caption">
            <div class="text-center">
                {content}
            </div>
        </div>
    </div>
</div>
"""


def read_player_file(filename, encoding='UTF-8'):
    with open(filename, encoding=encoding) as fd:
        reader = csv.DictReader(fd, delimiter=';', fieldnames=CSV_HEADERS)
        for i, row in enumerate(reader):
            if i == 0:
                continue
            yield row


def parse_photoset(source_dir):
    filenames = os.listdir(source_dir)

    photoset = {}

    for filename in filenames:
        groups = photo_re.match(filename)

        if not groups:
            continue

        photo_id = groups.group('id_player').upper().strip('- ')
        filepath = os.path.join(source_dir, filename)
        photoset[photo_id] = os.path.abspath(filepath)

    return photoset


def build_players(raw_data, photoset):
    for raw in raw_data:
        kwargs = {
            key: value
            for key, value in raw.items()
            if key in CSV_HEADERS}

        if not any(kwargs.values()):
            continue

        photo = photoset.get(kwargs['id_player'])
        yield Player(photo=photo, **kwargs)


class Player:
    def __init__(self,
                 id_trombi,
                 id_player,
                 group,
                 last_name,
                 first_name,
                 email,
                 chara_name,
                 photo=None,
                 mini_bg_sent=False,
                 bg_sent=False):
        self.id_trombi = id_trombi
        self.id_player = id_player
        self.group = group
        self.last_name = last_name
        self.first_name = first_name
        self.email = email
        self.chara_name = chara_name
        self.photo = photo
        """Picture's filename"""
        self.is_mini_bg_sent = mini_bg_sent
        self.is_bg_sent = bg_sent

    @property
    def full_name(self):
        return '{} {}'.format(
            self.last_name or '', self.first_name or ''
        ).strip()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate HTML face book for Agone LARP')
    parser.add_argument(
        '-i', '--input', dest='filename',
        default=os.path.abspath('characters.csv'),
        help='Path to the CSV file with player data '
            '(default to "%(default)s)"')
    parser.add_argument(
        '--charset', default='utf-8',
        help='Charset to read the player data (default to "%(default)s"')
    parser.add_argument(
        '-p', '--photo', dest='source_dir',
        default=os.path.abspath('photos'),
        help='Path to the folder with player\'s pictures '
            '(default to "%(default)s"')
    parser.add_argument(
        '-o', '--output', default=os.path.abspath('html'),
        help='Path to the output dir where '
            'HTML, CSS and photo will be created '
            '(default to %(default)s)')
    parser.add_argument(
        '--assets', default=os.path.abspath('assets'),
        help='Path to the assets folder (default to %(default)s)')

    options = parser.parse_args()

    filename = os.path.abspath(options.filename)
    source_dir = os.path.abspath(options.source_dir)
    output_dir = os.path.abspath(options.output)
    assets_dir = os.path.abspath(options.assets)

    if not os.path.isfile(filename):
        print('CSV file does not exist or is not readable:\n%s' % filename)
        exit(1)

    if not os.path.isdir(source_dir):
        print('Photo folder does not exist or is not readable:\n%s'
              % source_dir)
        exit(1)

    if not os.path.isdir(assets_dir):
        print('Assets folder does not exist or is not readable:\n%s'
              % assets_dir)
        exit(1)

    # Parse players
    player_data = sorted(
        read_player_file(filename, options.charset),
        key=lambda x: x['id_trombi'])
    # Parse photoset
    photoset = parse_photoset(source_dir)

    # Create dest folder
    os.makedirs(output_dir, exist_ok=True)

    # Print config
    print('CSV File:   ', filename)
    print('Photo Dir:  ', source_dir)
    print('Asset Dir:  ', assets_dir)
    print('Output Dir: ', output_dir)

    # Works
    content_players = ''
    content_orga = ''
    no_photo = os.path.join(assets_dir, 'no_photo.jpg')

    # Copy asset files
    print('Copy assets to %s...' % output_dir)
    shutil.copy(os.path.join(assets_dir, 'all.css'), output_dir)
    shutil.copy(os.path.join(assets_dir, 'bootstrap.min.css'), output_dir)
    shutil.copy(os.path.join(assets_dir, 'no_photo.jpg'), output_dir)

    # Generate HTML for each player
    for player in build_players(player_data, photoset):
        # Generate partial HTML
        player_no_details = html.escape(player.id_trombi)
        player_details = html.escape(' - '.join([
            str(player.id_trombi),
            str(player.id_player),
            str(player.chara_name),
            str(player.full_name),
        ]))

        # Get photo URL
        photo_name = html.escape(
            os.path.basename(player.photo or no_photo))

        # Copy player photo if exist
        if player.photo:
            shutil.copy(player.photo, output_dir)
        else:
            print('Player %s: photo not found' % player.id_player)

        content_players += TEMPLATE_LINE_PLAYER.format(
            photo=photo_name,
            content=player_no_details
        )
        content_orga += TEMPLATE_LINE_PLAYER.format(
            photo=photo_name,
            content=player_details
        )

    data_players = TEMPLATE_PLAYERS.format(content=content_players)
    data_orga = TEMPLATE_ORGAS.format(content=content_orga)

    # Write files
    filename_orga = os.path.join(output_dir, 'orga.html')
    filename_player = os.path.join(output_dir, 'player.html')

    print('Generate ORGA file: %s' % filename_orga)
    with open(filename_orga, 'w', encoding='utf-8') as fd:
        fd.write(data_orga)

    print('Generate PLAYER file: %s' % filename_player)
    with open(filename_player, 'w', encoding='utf-8') as fd:
        fd.write(data_players)
